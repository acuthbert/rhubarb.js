import {CustardCommand} from "./CustardCommand";

export abstract class Module {

    /**
     * Implement any custom bootstrapping logic in the initialise method.
     */
    public abstract initialise(): void;

    protected getCustardCommands(): CustardCommand[] {
        return [];
    }

    protected registerYargsCommands(): void {

    }

    public registerAllYargsCommands(): void {
        this.getAllModules().forEach(m => {
            m.registerYargsCommands();
        })
    }

    protected getDependantModules(): Module[] {
        return [];
    }

    public getAllModules(): Module[] {
        const response: Module[] = [];
        const modules = this.getDependantModules();
        modules.forEach(m => {
            response.push(m);
            const children = m.getAllModules();
            children.forEach(c => {
                response.push(c);
            })
        });

        return response;
    }
}